﻿using AthleteAPIDemo.Models.Domain;
using AthleteAPIDemo.Models.DTO.Coach;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AthleteAPIDemo.Profiles
{
    public class CoachProfile : Profile
    {
        public CoachProfile()
        {
            // Coach->CoachReadDTO
            CreateMap<Coach, CoachReadDTO>()
                // Turning related athletes into int arrays
                .ForMember(cdto => cdto.Athletes, opt => opt
                .MapFrom(c => c.Athletes.Select(c=>c.Id).ToArray()))
                // Turning related certifications into arrays
                .ForMember(cdto => cdto.Certifications, opt => opt
                .MapFrom(c => c.Certifications.Select(c => c.Id).ToArray()));
            // CoachCreateDTO->Coach
            CreateMap<CoachCreateDTO, Coach>();
            // CoachEditDTO->Coach
            CreateMap<CoachEditDTO, Coach>();

        }
    }
}
