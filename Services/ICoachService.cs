﻿using AthleteAPIDemo.Models.Domain;
using AthleteAPIDemo.Models.DTO.Coach;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AthleteAPIDemo.Services
{
    /// <summary>
    /// This service is essentially a repository, they are interchangable, still follows the same pattern.
    /// </summary>
    public interface ICoachService
    {
        // We can include a SaveChangesAsync method to have full control over EF, some implementaitons do this.
        // I am not for simplicity sake. Its technically more flexible.
        public Task<IEnumerable<Coach>> GetAllCoachesAsync();
        public Task<Coach> GetSpecificCoachAsync(int id);
        public Task<Coach> AddCoachAsync(Coach coach);
        public Task UpdateCoachAsync(Coach coach);
        public Task UpdateCoachCertificationsAsync(int coachId, List<int> certifications);
        public Task UpdateCoachAthletesAsync(int coachId, List<int> athletes);
        public Task DeleteCoachAsync(int id);
        public bool CoachExists(int id);
    }
}
