﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AthleteAPIDemo.Migrations
{
    public partial class seeding : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CertificationCoach");

            migrationBuilder.CreateTable(
                name: "CoachCertification",
                columns: table => new
                {
                    CoachId = table.Column<int>(type: "int", nullable: false),
                    CertificationId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CoachCertification", x => new { x.CoachId, x.CertificationId });
                    table.ForeignKey(
                        name: "FK_CoachCertification_Certification_CertificationId",
                        column: x => x.CertificationId,
                        principalTable: "Certification",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CoachCertification_Coach_CoachId",
                        column: x => x.CoachId,
                        principalTable: "Coach",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Certification",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Boxing" },
                    { 2, "Running" },
                    { 3, "Badass" }
                });

            migrationBuilder.InsertData(
                table: "Coach",
                columns: new[] { "Id", "Awards", "DOB", "Gender", "Name" },
                values: new object[,]
                {
                    { 1, 10, new DateTime(1981, 5, 31, 12, 11, 14, 182, DateTimeKind.Local).AddTicks(1352), "Male", "John McIntyre" },
                    { 2, 15, new DateTime(1991, 5, 31, 12, 11, 14, 184, DateTimeKind.Local).AddTicks(5824), "Female", "Renate Blindheim" },
                    { 3, 52, new DateTime(1947, 5, 31, 12, 11, 14, 184, DateTimeKind.Local).AddTicks(5872), "Male", "Phil Jackson" },
                    { 4, 38, new DateTime(1986, 5, 31, 12, 11, 14, 184, DateTimeKind.Local).AddTicks(5888), "Female", "Christine Girard" }
                });

            migrationBuilder.InsertData(
                table: "Athlete",
                columns: new[] { "Id", "CoachId", "DOB", "Gender", "Name", "Records" },
                values: new object[,]
                {
                    { 4, 1, new DateTime(1975, 5, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), "Male", "David Beckham", 52 },
                    { 5, 1, new DateTime(1986, 8, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), "Male", "Usain Bolt", 42 },
                    { 3, 2, new DateTime(1971, 9, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), "Male", "Kjetil Andre Aamodt", 28 },
                    { 1, 3, new DateTime(1963, 2, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), "Male", "Micheal Jordan", 10 },
                    { 2, 4, new DateTime(1971, 10, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), "Male", "Thomas Ulsrud", 15 }
                });

            migrationBuilder.InsertData(
                table: "CoachCertification",
                columns: new[] { "CertificationId", "CoachId" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 2, 1 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_CoachCertification_CertificationId",
                table: "CoachCertification",
                column: "CertificationId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CoachCertification");

            migrationBuilder.DeleteData(
                table: "Athlete",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Athlete",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Athlete",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Athlete",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Athlete",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Certification",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Certification",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Certification",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Coach",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Coach",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Coach",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Coach",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.CreateTable(
                name: "CertificationCoach",
                columns: table => new
                {
                    CertificationsId = table.Column<int>(type: "int", nullable: false),
                    CoachesId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CertificationCoach", x => new { x.CertificationsId, x.CoachesId });
                    table.ForeignKey(
                        name: "FK_CertificationCoach_Certification_CertificationsId",
                        column: x => x.CertificationsId,
                        principalTable: "Certification",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CertificationCoach_Coach_CoachesId",
                        column: x => x.CoachesId,
                        principalTable: "Coach",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CertificationCoach_CoachesId",
                table: "CertificationCoach",
                column: "CoachesId");
        }
    }
}
