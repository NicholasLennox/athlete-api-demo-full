﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AthleteAPIDemo.Models;
using AthleteAPIDemo.Models.Domain;
using System.Net.Mime;
using AutoMapper;
using AthleteAPIDemo.Models.DTO.Athlete;

namespace AthleteAPIDemo.Controllers
{
    [Route("api/v1/athletes")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class AthletesController : ControllerBase
    {
        private readonly AthleteDbContext _context;
        // Add automapper via DI
        private readonly IMapper _mapper;

        public AthletesController(AthleteDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets all the athletes in the database.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<AthleteReadDTO>>> GetAthletes()
        {
            return _mapper.Map<List<AthleteReadDTO>>(await _context.Athletes.ToListAsync());
        }

        /// <summary>
        /// Gets a specific athlete by their Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<AthleteReadDTO>> GetAthlete(int id)
        {
            var athlete = await _context.Athletes.FindAsync(id);

            if (athlete == null)
            {
                return NotFound();
            }

            return _mapper.Map<AthleteReadDTO>(athlete);
        }

        /// <summary>
        /// Updates an athlete.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="athlete"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAthlete(int id, AthleteEditDTO athlete)
        {
            if (id != athlete.Id)
            {
                return BadRequest();
            }
            Athlete domainAthlete = _mapper.Map<Athlete>(athlete);
            _context.Entry(domainAthlete).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AthleteExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Adds a new athlete to the database.
        /// </summary>
        /// <param name="athlete"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Athlete>> PostAthlete(Athlete athlete)
        {
            _context.Athletes.Add(athlete);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetAthlete", new { id = athlete.Id }, athlete);
        }

        /// <summary>
        /// Deletes an athlete.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAthlete(int id)
        {
            var athlete = await _context.Athletes.FindAsync(id);
            if (athlete == null)
            {
                return NotFound();
            }

            _context.Athletes.Remove(athlete);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool AthleteExists(int id)
        {
            return _context.Athletes.Any(e => e.Id == id);
        }
    }
}
