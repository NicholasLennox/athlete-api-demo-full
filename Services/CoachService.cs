﻿using AthleteAPIDemo.Models;
using AthleteAPIDemo.Models.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AthleteAPIDemo.Services
{
    // Our coach service depends on DbContext, frees up our controllers to change implementation for testing.
    // We could try catch and rethrow errors with messages here, but its caught in the controller anyway, so its not needed.
    public class CoachService : ICoachService
    {
        private readonly AthleteDbContext _context;

        public CoachService(AthleteDbContext context)
        {
            _context = context;
        }

        public async Task<Coach> AddCoachAsync(Coach coach)
        {
            _context.Coaches.Add(coach);
            await _context.SaveChangesAsync();
            return coach;
        }

        public bool CoachExists(int id)
        {
            return _context.Coaches.Any(e => e.Id == id);
        }

        public async Task DeleteCoachAsync(int id)
        {
            var coach = await _context.Coaches.FindAsync(id);
            _context.Coaches.Remove(coach);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Coach>> GetAllCoachesAsync()
        {
            return await _context.Coaches
                .Include(c => c.Athletes)
                .Include(c => c.Certifications)
                .ToListAsync();
        }

        public async Task<Coach> GetSpecificCoachAsync(int id)
        {
            return await _context.Coaches.FindAsync(id);
        }

        public async Task UpdateCoachAsync(Coach coach)
        {
            _context.Entry(coach).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task UpdateCoachAthletesAsync(int coachId, List<int> athletes)
        {
            throw new NotImplementedException();
        }

        public async Task UpdateCoachCertificationsAsync(int coachId, List<int> certifications)
        {
            Coach coachToUpdateCerts = await _context.Coaches
                .Include(c => c.Certifications)
                .Where(c => c.Id == coachId)
                .FirstAsync();

            // Loop through certificates, try and assign to coach
            // Trying to see if there is a nicer way of doing this, dont like the multiple calls
            List<Certification> certs = new();
            foreach (int certId in certifications)
            {
                Certification cert = await _context.Certifications.FindAsync(certId);
                if (cert == null)
                    // Record doesnt exist: https://docs.microsoft.com/en-us/previous-versions/dotnet/netframework-4.0/ms229021(v=vs.100)?redirectedfrom=MSDN
                    throw new KeyNotFoundException();
                certs.Add(cert);
            }
            coachToUpdateCerts.Certifications = certs;
            await _context.SaveChangesAsync();
        }
    }
}
