﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AthleteAPIDemo.Models.DTO.Certification
{
    public class CertificationReadDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
