﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AthleteAPIDemo.Models.Domain
{
    [Table("Coach")]
    public class Coach
    {
        // PK
        public int Id { get; set; }
        // Fields
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
        public DateTime DOB { get; set; }
        [MaxLength(50)]
        public string Gender { get; set; }
        public int Awards { get; set; }
        // Relationships
        public ICollection<Athlete> Athletes { get; set; }
        public ICollection<Certification> Certifications { get; set; }

    }
}
