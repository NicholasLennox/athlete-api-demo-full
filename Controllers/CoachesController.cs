﻿using AthleteAPIDemo.Models;
using AthleteAPIDemo.Models.Domain;
using AthleteAPIDemo.Models.DTO.Coach;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace AthleteAPIDemo.Controllers
{
    [Route("api/v1/coaches")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class CoachesController : ControllerBase
    {
        private readonly AthleteDbContext _context;
        // Add automapper via DI
        private readonly IMapper _mapper;

        public CoachesController(AthleteDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        #region Generic CRUD with DTOs

        /// <summary>
        /// Fetches all the coaches.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CoachReadDTO>>> GetCoaches()
        {
            return _mapper.Map<List<CoachReadDTO>>(await _context.Coaches
                .Include(c=>c.Athletes)
                .Include(c=>c.Certifications)
                .ToListAsync());
        }

        /// <summary>
        /// Gets a specific coach by thier Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CoachReadDTO>> GetCoach(int id)
        {
            Coach coach = await _context.Coaches.FindAsync(id);

            if (coach == null)
            {
                return NotFound();
            }

            return _mapper.Map<CoachReadDTO>(coach);
        }

        /// <summary>
        /// Updates a coach. Must pass a full coach object and Id in route.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="dtoCoach"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCoach(int id, CoachEditDTO dtoCoach)
        {
            if (id != dtoCoach.Id)
            {
                return BadRequest();
            }

            Coach domainCoach = _mapper.Map<Coach>(dtoCoach);

            _context.Entry(domainCoach).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CoachExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Adds a new coach to the database.
        /// </summary>
        /// <param name="dtoCoach"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Coach>> PostCoach(CoachCreateDTO dtoCoach)
        {
            Coach domainCoach = _mapper.Map<Coach>(dtoCoach);
            _context.Coaches.Add(domainCoach);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCoach", 
                new { id = domainCoach.Id }, 
                _mapper.Map<CoachReadDTO>(domainCoach));
        }

        /// <summary>
        /// Deletes a coach from the database.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCoach(int id)
        {
            var coach = await _context.Coaches.FindAsync(id);
            if (coach == null)
            {
                return NotFound();
            }

            _context.Coaches.Remove(coach);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool CoachExists(int id)
        {
            return _context.Coaches.Any(e => e.Id == id);
        }

        #endregion

        #region Just updating related coach information

        [HttpPut("{id}/certificates")]
        public async Task<IActionResult> UpdateCoachCertificates(int id, List<int> certificates)
        {
            if (!CoachExists(id))
            {
                return NotFound();
            }

            // May want to place this in a service - controller is getting bloated
            Coach coachToUpdateCerts = await _context.Coaches
                .Include(c=>c.Certifications)
                .Where(c=> c.Id == id)
                .FirstAsync();

            // Loop through certificates, try and assign to coach
            // Trying to see if there is a nicer way of doing this, dont like the multiple calls
            List<Certification> certs = new();
            foreach (int certId in certificates)
            {
                Certification cert = await _context.Certifications.FindAsync(certId);
                if (cert == null)
                    return BadRequest("Certification doesnt exist!");
                certs.Add(cert);
            }
            coachToUpdateCerts.Certifications = certs;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return NoContent();
        }

        [HttpPut("{id}/athletes")]
        public async Task<IActionResult> UpdateCoachAthletes(int id, List<int> athletes)
        {
            return NoContent();
        }

        #endregion
    }
}
