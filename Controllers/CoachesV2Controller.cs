﻿using AthleteAPIDemo.Models;
using AthleteAPIDemo.Models.Domain;
using AthleteAPIDemo.Models.DTO.Coach;
using AthleteAPIDemo.Services;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace AthleteAPIDemo.Controllers
{
    /// <summary>
    /// The same as coaches controller but with a service.
    /// </summary>
    [Route("api/v2/coaches")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class CoachesV2Controller : ControllerBase
    {
        // Add automapper via DI
        private readonly IMapper _mapper;
        // Controllers still are responsible for mapping, services/repositories work with domain objects
        private readonly ICoachService _coachService;
        // We no longer are dependent on the entire AthleteDbContext

        public CoachesV2Controller(IMapper mapper, ICoachService coachService)
        {
            _mapper = mapper;
            _coachService = coachService;
        }

        #region Generic CRUD with DTOs

        /// <summary>
        /// Fetches all the coaches.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CoachReadDTO>>> GetCoaches()
        {
            return _mapper.Map<List<CoachReadDTO>>(await _coachService.GetAllCoachesAsync());
        }

        /// <summary>
        /// Gets a specific coach by thier Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CoachReadDTO>> GetCoach(int id)
        {
            Coach coach = await _coachService.GetSpecificCoachAsync(id);

            if (coach == null)
            {
                return NotFound();
            }

            return _mapper.Map<CoachReadDTO>(coach);
        }

        /// <summary>
        /// Updates a coach. Must pass a full coach object and Id in route.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="dtoCoach"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCoach(int id, CoachEditDTO dtoCoach)
        {
            if (id != dtoCoach.Id)
            {
                return BadRequest();
            }

            if (!_coachService.CoachExists(id))
            {
                return NotFound();
            }

            Coach domainCoach = _mapper.Map<Coach>(dtoCoach);
            await _coachService.UpdateCoachAsync(domainCoach);

            return NoContent();
        }

        /// <summary>
        /// Adds a new coach to the database.
        /// </summary>
        /// <param name="dtoCoach"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Coach>> PostCoach(CoachCreateDTO dtoCoach)
        {
            Coach domainCoach = _mapper.Map<Coach>(dtoCoach);

            domainCoach = await _coachService.AddCoachAsync(domainCoach);

            return CreatedAtAction("GetCoach", 
                new { id = domainCoach.Id }, 
                _mapper.Map<CoachReadDTO>(domainCoach));
        }

        /// <summary>
        /// Deletes a coach from the database.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCoach(int id)
        {
            if (!_coachService.CoachExists(id))
            {
                return NotFound();
            }

            await _coachService.DeleteCoachAsync(id);

            return NoContent();
        }

        #endregion

        #region Just updating related coach information

        [HttpPut("{id}/certificates")]
        public async Task<IActionResult> UpdateCoachCertificates(int id, List<int> certificates)
        {
            if (!_coachService.CoachExists(id))
            {
                return NotFound();
            }

            try
            {
                await _coachService.UpdateCoachCertificationsAsync(id, certificates);
            } catch (KeyNotFoundException)
            {
                return BadRequest("Invalid certification.");
            }
                      
            return NoContent();
        }

        [HttpPut("{id}/athletes")]
        public async Task<IActionResult> UpdateCoachAthletes(int id, List<int> athletes)
        {
            return NoContent();
        }

        #endregion
    }
}
