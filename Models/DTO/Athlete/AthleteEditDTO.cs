﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AthleteAPIDemo.Models.DTO.Athlete
{
    public class AthleteEditDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime DOB { get; set; }
        public string Gender { get; set; }
        public int Records { get; set; }
        public int Coach { get; set; }
    }
}
