﻿using AthleteAPIDemo.Models.Domain;
using AthleteAPIDemo.Models.DTO.Athlete;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AthleteAPIDemo.Profiles
{
    public class AthleteProfile : Profile
    {
        public AthleteProfile()
        {
            // Athlete<->AthleteReadDTO
            CreateMap<Athlete, AthleteReadDTO>()
                .ForMember(adto=> adto.Coach, opt => opt
                .MapFrom(a => a.CoachId))
                .ReverseMap();
            // Athlete<->AthleteCreateDTO
            CreateMap<Athlete, AthleteCreateDTO>()
                .ForMember(adto => adto.Coach, opt => opt
                .MapFrom(a => a.CoachId))
                .ReverseMap();
            // Athlete<->AtheleteEditDTO
            CreateMap<Athlete, AthleteEditDTO>()
                .ForMember(adto => adto.Coach, opt => opt
                .MapFrom(a => a.CoachId))
                .ReverseMap();
        }
    }
}
