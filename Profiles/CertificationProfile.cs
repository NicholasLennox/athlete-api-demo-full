﻿using AthleteAPIDemo.Models.Domain;
using AthleteAPIDemo.Models.DTO.Certification;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AthleteAPIDemo.Profiles
{
    public class CertificationProfile : Profile
    {
        public CertificationProfile()
        {
            // Certification<->CertificationReadDTO
            CreateMap<Certification, CertificationReadDTO>()
                .ReverseMap();
            // Certification<->CertificationCreateDTO
            CreateMap<Certification, CertificationCreateDTO>()
                .ReverseMap();
        }
    }
}
